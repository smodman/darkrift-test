﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using DarkRift.Server.Unity;
using DarkRift.Server;
using DarkRift;
using DarkriftSerializationExtensions;
using DarkRift.Extension.Server;
using DarkRift.Extension.Message;


public class ServerManager : MonoBehaviour
{
    public static ServerManager Instance;

    Dictionary<IClient, Player> players = new Dictionary<IClient, Player>();
    public ServerObjectDictionary serverObjects = new ServerObjectDictionary();
    private readonly Dictionary<ClientTag, Action<MessageReceivedEventArgs>> messageHandler = new Dictionary<ClientTag, Action<MessageReceivedEventArgs>>();

    public XmlUnityServer XmlServer;
    public DarkRiftServer Server;

    private readonly NetIDManager netIDManager = new NetIDManager();

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Initiation function that must be called before any client connections occur.
    /// </summary>
    public void Init()
    {
        Debug.Log("Server initiated.");
        Server = XmlServer.Server;
        Server.ClientManager.ClientConnected += ClientConnected;
        Server.ClientManager.ClientDisconnected += ClientDisconnected;
        messageHandler.Add(ClientTag.SceneLoaded, SceneLoaded);
        messageHandler.Add(ClientTag.ObjectMove, ObjectMove);
    }

    private void FixedUpdate()
    {
        GameUpdateMessage gameUpdateMessage = new GameUpdateMessage();
         
        Server.ClientManager.GetAllClients().SendTcpMessageToAll(new GameUpdateMessage(serverObjects.Values.ToArray()));
    }

    void ClientConnected(object sender, ClientConnectedEventArgs e)
    {
        Debug.Log("Client connected.");
        Player newPlayer = new Player(
            e.Client.ID, netIDManager.GetNext(), new Vector3(0.0f, 0.0f, 0.0f));

        players.Add(e.Client, newPlayer);
        serverObjects.Add(newPlayer.NetID, new ServerObject(newPlayer.NetID, newPlayer.Position));

        e.Client.MessageReceived += MessageReceived;
    }

    void ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
    {
        players.Remove(e.Client);

        e.Client.MessageReceived -= MessageReceived;
    }

    void MessageReceived(object sender, MessageReceivedEventArgs e)
    {
        using (Message message = e.GetMessage() as Message)
        {
            Debug.LogFormat("Server received message from Client {0} with Tag: {1} {2}", e.Client.ID, e.Tag, (ClientTag)e.Tag);
            Action<MessageReceivedEventArgs> method;
            if (messageHandler.TryGetValue((ClientTag)e.Tag, out method))
            {
                method(e);
            }
            else
            {
                Debug.LogError(string.Format("Server Message Handler: No such tag: {0}", e.Tag));
            }
        }
    }

    void SceneLoaded(MessageReceivedEventArgs e)
    {
        Debug.Log("SceneLoaded method");
        SceneLoadedMessage sceneLoadedMessage = e.ExtractMessage<SceneLoadedMessage>();

        Debug.LogFormat("{0}: Scene Loaded Message received from client: {1} with tag: {2} & random int: {3}", DateTime.Now.ToString(), e.Client.ID, e.Tag, sceneLoadedMessage.randomValue);

        players[e.Client].Position = sceneLoadedMessage.position;

        PlayerSpawnMessage playerSpawnMessage = new PlayerSpawnMessage(sceneLoadedMessage.ClientId, players[e.Client].NetID, sceneLoadedMessage.position);

        /// Tell everyone else to spawn me!!!
        Server.ClientManager.GetAllClients().SendTcpMessageToAllExcept<ServerTag>(playerSpawnMessage, new List<IClient> { e.Client});

        /// Send NetID of all network objects

        /// Spawn all players (including himself) to player who just joined
        foreach (Player player in players.Values)
        {
            e.Client.SendTcpMessage(new PlayerSpawnMessage(player.ClientID, player.NetID, player.Position));
        }
    }

    void ObjectMove(MessageReceivedEventArgs e)
    {
        MoveMessage moveMessage = e.ExtractMessage<MoveMessage>();
        ServerObject so;
        if (serverObjects.TryGetValue(moveMessage.netId, out so)){
            // If movement message was a player movement update
            so.position = moveMessage.position;
            serverObjects[moveMessage.netId] = so;
        }
        else
        {
            Debug.Log("Net ID does not exist in server objects.");
        }
    }

    private class Player : IDarkRiftSerializable
    {
        public ushort ClientID { get; set; }
        public int NetID { get; set; }
        public Vector3 Position { get; set; }

        public Player()
        {

        }

        public Player(ushort iD, int NetID, Vector3 position)
        {
            this.ClientID = iD;
            this.NetID = NetID;
            this.Position = position;
        }

        public void Deserialize(DeserializeEvent e)
        {
            this.ClientID = e.Reader.ReadUInt16();
            this.NetID = e.Reader.ReadInt32();
            this.Position = e.Reader.ReadVector3();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(ClientID);
            e.Writer.Write(NetID);
            e.Writer.WriteVector3(this.Position);
        }
    }

    public struct ObjectMoveData : IDarkRiftSerializable
    {
        public int NetID;
        public Vector3 Position;

        public ObjectMoveData(int netID, Vector3 position)
        {
            this.NetID = netID;
            this.Position = position;
        }

        public void Deserialize(DeserializeEvent e)
        {
            this.NetID = e.Reader.ReadInt32();
            this.Position = new Vector3(e.Reader.ReadSingle(), e.Reader.ReadSingle(), e.Reader.ReadSingle());
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(NetID);
            e.Writer.WriteVector3(Position);
        }
    }
}