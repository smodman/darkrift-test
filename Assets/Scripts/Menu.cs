﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DarkRift.Client.Unity;
using DarkRift;
using DarkRift.Server.Unity;
using DarkRift.Server;
using System.Net;

public class Menu : MonoBehaviour
{
    public InputField ip;
    public InputField port;

    public GameObject ClientManager;
    public GameObject ServerManager;
    public GameObject PrefabManager;

    public void Connect()
    {
        GameObject prefabManager = Instantiate(PrefabManager, Vector3.zero, Quaternion.identity);
        GameObject client = Instantiate(ClientManager, Vector3.zero, Quaternion.identity);
        client.GetComponent<UnityClient>().Connect(IPAddress.Parse(ip.text), int.Parse(port.text), IPVersion.IPv4);
        SceneManager.LoadScene("Pixel3D", LoadSceneMode.Single);
    }

    public void Host()
    {
        GameObject prefabManager = Instantiate(PrefabManager, Vector3.zero, Quaternion.identity);
        GameObject server = Instantiate(ServerManager, Vector3.zero, Quaternion.identity);
        server.GetComponent<ServerManager>().Init();
        GameObject client = Instantiate(ClientManager, Vector3.zero, Quaternion.identity);
        client.GetComponent<ClientManager>().IsHost = true;
        client.GetComponent<UnityClient>().Connect(IPAddress.Parse(ip.text), int.Parse(port.text), IPVersion.IPv4);
        SceneManager.LoadScene("Pixel3D", LoadSceneMode.Single);
    }
}
