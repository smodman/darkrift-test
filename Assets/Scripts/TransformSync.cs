﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Client.Unity;
using DarkRift.Client;
using DarkRift;
using DarkriftSerializationExtensions;
using DarkRift.Extension.Client;

[RequireComponent(typeof(NetworkObject))]
public class TransformSync : MonoBehaviour
{
    float moveDistance = 0.05f;

    // Last position sent to the server
    Vector3 lastPosition;

    NetworkObject no;

    void Awake()
    {
        lastPosition = transform.position;
        no = GetComponent<NetworkObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (no.isOwner && no.NetID != 0)
        {
            if (Vector3.Distance(lastPosition, transform.position) > moveDistance)
            {
                ClientManager.Instance.client.Client.SendMessageReliable(new MoveMessage(no.NetID, transform.position));

                lastPosition = transform.position;
            }
        }
    }
}
