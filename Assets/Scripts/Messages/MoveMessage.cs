﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Extension.Message.Core;
using DarkRift;
using DarkriftSerializationExtensions;

public sealed class MoveMessage : IDarkRiftMessage<ClientTag>
{
    public ClientTag Tag => ClientTag.ObjectMove;

    public int netId;
    public Vector3 position;

    public MoveMessage(int netId, Vector3 position)
    {
        this.netId = netId;
        this.position = position;
    }

    public MoveMessage()
    {
    }

    public void Deserialize(DeserializeEvent e)
    {
        this.netId = e.Reader.ReadInt32();
        this.position = e.Reader.ReadVector3();
    }

    public void Serialize(SerializeEvent e)
    {
        e.Writer.Write(netId);
        e.Writer.WriteVector3(position);
    }
}
