﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Extension.Message.Core;
using DarkRift;
using DarkriftSerializationExtensions;
using System;

public sealed class SceneLoadedMessage : IDarkRiftMessage<ClientTag>
{
    public ClientTag Tag => ClientTag.SceneLoaded;

    public ushort ClientId;
    public Vector3 position;
    public int randomValue;

    public SceneLoadedMessage(ushort clientId, Vector3 position)
    {
        ClientId = clientId;
        this.position = position;
        this.randomValue = UnityEngine.Random.Range(0, 1000);
    }

    public SceneLoadedMessage()
    {
    }

    public void Deserialize(DeserializeEvent e)
    {
        ClientId = e.Reader.ReadUInt16();
        position = e.Reader.ReadVector3();
        randomValue = e.Reader.ReadInt32();
    }

    public void Serialize(SerializeEvent e)
    {
        e.Writer.Write(ClientId);
        e.Writer.WriteVector3(position);
        e.Writer.Write(randomValue);
    }
}
