﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Client.Unity;
using DarkRift.Client;
using DarkRift;
using DarkRift.Extension.Message;
using System;

public class ClientManager : MonoBehaviour
{
    private static ClientManager _instance;

    public static ClientManager Instance { get { return _instance; } }

    [SerializeField]
    [Tooltip("The DarkRift client to communicate on.")]
    public UnityClient client;

    [SerializeField]
    [Tooltip("The player prefab.")]
    GameObject player;

    public NetworkObjectDictionary networkObjects = new NetworkObjectDictionary();
    private readonly Dictionary<ServerTag, Action<MessageReceivedEventArgs>> messageHandler = new Dictionary<ServerTag, Action<MessageReceivedEventArgs>>();

    public bool IsHost { get; set; }

    public NetworkObject myPlayer;

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if (client == null)
        {
            Debug.LogError("Client unassigned in PlayerSpawner.");
            Application.Quit();
        }

        if (player == null)
        {
            Debug.LogError("Player Prefab unassigned in PlayerSpawner.");
            Application.Quit();
        }

        DontDestroyOnLoad(this);

        client.MessageReceived += MessageReceived;
        messageHandler.Add(ServerTag.PlayerSpawn, SpawnPlayer);
        messageHandler.Add(ServerTag.GameUpdate, ApplyGameState);
    }

    void MessageReceived(object sender, MessageReceivedEventArgs e)
    {
        using (Message message = e.GetMessage() as Message)
        {
            Action<MessageReceivedEventArgs> method;
            if (messageHandler.TryGetValue((ServerTag)e.Tag, out method))
            {
                method(e);
            }
            else
            {
                Debug.LogError(string.Format("No such tag: {0}", e.Tag));
            }
        }
    }

    void ApplyGameState(MessageReceivedEventArgs e)
    {
        GameUpdateMessage gameUpdateMessage = e.ExtractMessage<GameUpdateMessage>();

        foreach (ServerObject serverObject in gameUpdateMessage.serverObjectData)
        {
            NetworkObject no;
            if (networkObjects.TryGetValue(serverObject.netId, out no)) {
                //Debug.LogFormat("Moving network object {0} to position {1}", networkObjectUpdateData.netId, networkObjectUpdateData.position);
                if (no.Interpolation != null)
                {
                    no.Interpolation.MoveToPosition(serverObject.position);
                }
            }
            else
            {
                Debug.LogWarningFormat("Network object with ID {0} does not exist!", serverObject.netId);
            }
        }
    }

    void SpawnPlayer(MessageReceivedEventArgs e)
    {
        PlayerSpawnMessage playerSpawnMessage = e.ExtractMessage<PlayerSpawnMessage>();
        GameObject obj = Instantiate(player, playerSpawnMessage.position, Quaternion.identity) as GameObject;
        if (playerSpawnMessage.ClientId == client.ID)
        {
            FindObjectOfType<CameraFollowTopDown>().target = obj;
        }

        NetworkObject networkObj = obj.GetComponent<NetworkObject>();
        networkObj.OwnerID = playerSpawnMessage.ClientId;
        networkObj.NetID = playerSpawnMessage.netId;
        myPlayer = networkObj;

        networkObjects.Add(playerSpawnMessage.netId, networkObj);
    }
}
