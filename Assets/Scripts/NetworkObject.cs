﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NetworkObject : MonoBehaviour
{
    [SerializeField]
    public int NetID;

    [SerializeField]
    public int OwnerID;

    private TextMeshPro textMesh;

    public bool isOwner { get { return OwnerID == ClientManager.Instance.client.ID; } }

    public CharacterInterpolation Interpolation;

    void Awake()
    {
        GameObject child = new GameObject();
        child.transform.position = gameObject.transform.position;
        child.transform.parent = gameObject.transform;
        textMesh = child.AddComponent<TextMeshPro>();
        textMesh.alignment = TextAlignmentOptions.Top;
        textMesh.fontSize = 5.0f;
    }

    void Start()
    {
        Interpolation = GetComponent<CharacterInterpolation>();
    }

    void Update()
    {
        textMesh.SetText("NetID: {0}", NetID);
    }

    private void OnDestroy()
    {
        if (ClientManager.Instance.IsHost)
        {
           //ServerManager.Instance.RemoveServerObject(NetID);
           // ClientManager.Instance.RemoveClientNetworkObject(NetID);
        }
        else
        {
           // ClientManager.Instance.RemoveClientNetworkObject(NetID);
        }
    }
}
