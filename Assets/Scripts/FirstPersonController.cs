﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    public float moveSpeed = 5.0f;
    public float mouseSensitivity = 5.0f;
    public float maxPitch = 60.0f;
    public float jumpSpeed = 5.0f;
    float verticalRot = 0;
    public float rotationSpeed = 2.0f;

    float verticalVelocity = 0;

    CharacterController cc;
    Animator anim;
    NetworkObject no;

    // Use this for initialization
    void Start()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        cc = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        no = GetComponent<NetworkObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!no.isOwner)
        {
            return;
        }

        // Movement
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(0, -rotationSpeed, 0);
        }
        else if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(0, rotationSpeed, 0);
        }

        float forwardSpeed = Input.GetAxisRaw("Vertical") * moveSpeed;
        float sideSpeed = Input.GetAxisRaw("Horizontal") * moveSpeed;

        //verticalVelocity += Physics.gravity.y * Time.deltaTime;

        //if (cc.isGrounded && Input.GetButtonDown("Jump"))
           // verticalVelocity = jumpSpeed;

        Vector3 velocity = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);

        velocity = transform.rotation * velocity;

        cc.Move(velocity * Time.deltaTime);

        bool moving = velocity.x != 0.0f || velocity.z != 0.0f ? true : false;
        anim.SetBool("Moving", moving);
    }
}
