﻿using System;

public enum ClientTag : ushort {
    SceneLoaded = 0,
    ObjectMove
}