﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Extension.Message.Core;
using DarkRift;
using DarkriftSerializationExtensions;

public sealed class PlayerSpawnMessage : IDarkRiftMessage<ServerTag>
{
    public ServerTag Tag => ServerTag.PlayerSpawn;

    public ushort ClientId;
    public int netId;
    public Vector3 position;

    public PlayerSpawnMessage(ushort clientId, int netId, Vector3 position)
    {
        ClientId = clientId;
        this.netId = netId;
        this.position = position;
    }

    public PlayerSpawnMessage()
    {
    }

    public void Deserialize(DeserializeEvent e)
    {
        this.ClientId = e.Reader.ReadUInt16();
        this.netId = e.Reader.ReadInt32();
        this.position = e.Reader.ReadVector3();
    }

    public void Serialize(SerializeEvent e)
    {
        e.Writer.Write(ClientId);
        e.Writer.Write(netId);
        e.Writer.WriteVector3(position);
    }
}
