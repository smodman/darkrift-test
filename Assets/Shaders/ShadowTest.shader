﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)


// Default Unity sprite shader with a second pass for offset shadow
// Edit by @_Dickie

Shader "Sprites/Default Shadowed"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
		[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0

		_HorizontalSkew("Horizontal Skew", Float) = 0
		_VerticalSkew("Vertical Skew", Float) = 0

		_ScaleX("Scale X", Float) = 1.0
		_ScaleY("Scale Y", Float) = 1.0

		_ShadowDistance("Shadow Distance", vector) = (-0.1,-0.1,0.1,0)
		_ShadowColor("Shadow Color", color) = (0.05,0.1,0.25,0.9)
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		 //shadow pass
		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma target 2.0
#pragma multi_compile_instancing
#pragma multi_compile _ PIXELSNAP_ON
#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
#include "UnitySprites.cginc"

		float4 _ShadowDistance;
	half4 _ShadowColor;
	float _HorizontalSkew;
	float _VerticalSkew;
	uniform float _ScaleX;
	uniform float _ScaleY;

	v2f vert(appdata_t IN)
	{
		v2f OUT;

		UNITY_SETUP_INSTANCE_ID(IN);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

#ifdef UNITY_INSTANCING_ENABLED
		IN.vertex.xy *= _Flip.xy;
#endif

		float h = _HorizontalSkew;
		float v = _VerticalSkew;
		float4x4 transformMatrix = float4x4(
			1, h, 0, 0,
			v, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);

		IN.vertex = mul(transformMatrix, IN.vertex);

		OUT.vertex = mul(UNITY_MATRIX_P,
			mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0))
			+ float4(IN.vertex.x, IN.vertex.y, 0.0, 0.0)
			* float4(_ScaleX, _ScaleY, 1.0, 1.0));

		//OUT.vertex = UnityObjectToClipPos(IN.vertex);
		OUT.texcoord = IN.texcoord;
		OUT.color = IN.color * _Color * _RendererColor;

		OUT.vertex += mul(UNITY_MATRIX_P, _ShadowDistance); // offsetting the shadow pass by shadow distance

#ifdef PIXELSNAP_ON
		OUT.vertex = UnityPixelSnap(OUT.vertex);
#endif

		return OUT;
	}

	fixed4 frag(v2f IN) : SV_Target
	{
		fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;

		c.rgb = _ShadowColor.rgb;  // replace texture colour with shadow colour
		c.rgb *= c.a;
		c *= _ShadowColor.a; // multiply it all by shadow's alpha colour so you can have transparent shadows

	return c;
	}

		ENDCG
	}

		// standard pass
		Pass
	{
		CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#pragma target 2.0
	#pragma multi_compile_instancing
	#pragma multi_compile _ PIXELSNAP_ON
	#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
	#include "UnityCG.cginc"

		uniform float _ScaleX;
		uniform float _ScaleY;

		appdata_img vert(appdata_img IN)
		{
			appdata_img OUT;

			OUT.vertex = mul(UNITY_MATRIX_P,
				mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0))
				+ float4(IN.vertex.x, IN.vertex.y, 0.0, 0.0)
				* float4(_ScaleX, _ScaleY, 1.0, 1.0));

			OUT.texcoord = IN.texcoord;

			return OUT;
		}

		sampler2D _MainTex;
		fixed4 frag(appdata_img IN) : COLOR
		{
			fixed4 color = tex2D(_MainTex, IN.texcoord);
		color.rgb *= color.a;

		return color;
		}

		ENDCG
	}
	}
}