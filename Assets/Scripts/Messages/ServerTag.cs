﻿using System;

public enum ServerTag : ushort
{
    PlayerSpawn = 0,
    GameUpdate
}
