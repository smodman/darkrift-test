﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift;

public class NetIDManager
{
    private int currentId = 1;

    public int GetNext()
    {
        return currentId++;
    }
}
