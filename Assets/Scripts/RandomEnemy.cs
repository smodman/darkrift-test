﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEnemy : MonoBehaviour {

    Vector3 startPosition;

    public float maxDistance = 20.0f;
    public float speed = 10.0f;

    public float cooldown = 5.0f;
    private float timer = 0.0f;

    private Vector3 target;
    private Vector3 targetDirection;
    private Vector3 movePosition;

    Rigidbody rb;
    NetworkObject no;

    // Use this for initialization
    void Awake () {

        Random.InitState(System.DateTime.Now.Millisecond);
        target = transform.position;
        movePosition = transform.position;
        rb = GetComponent<Rigidbody>();
        no = GetComponent<NetworkObject>();
    }

    // Update is called once per frame
    void Update() {

        if (!no.isOwner)
        {
            return;
        }

        timer += Time.deltaTime;

        if (timer >= cooldown)
        {
            FreeRoam();
            timer = 0.0f;
        }
	}

    private void FixedUpdate()
    {
        movePosition = rb.position + targetDirection * speed * Time.fixedDeltaTime;
        rb.MovePosition(movePosition);
    }

    void FreeRoam()
    {
        Vector3 randomDirection = transform.position + Random.insideUnitSphere * maxDistance;
        randomDirection = new Vector3(randomDirection.x, transform.position.y, randomDirection.z);
        target = randomDirection;
        targetDirection = target - transform.position;
        targetDirection.Normalize();
    }

}
