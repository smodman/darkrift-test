﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using System;
using DarkRift;
using DarkriftSerializationExtensions;

[Serializable]
public class ServerObjectDictionary : SerializableDictionaryBase<int, ServerObject>
{
}

[Serializable]
public struct ServerObject : IDarkRiftSerializable
{
    public int netId;
    public Vector3 position;

    public ServerObject(int netId, Vector3 position)
    {
        this.netId = netId;
        this.position = position;
    }

    public void Deserialize(DeserializeEvent e)
    {
        this.netId = e.Reader.ReadInt32();
        this.position = e.Reader.ReadVector3();
    }

    public void Serialize(SerializeEvent e)
    {
        e.Writer.Write(netId);
        e.Writer.WriteVector3(position);
    }
}
