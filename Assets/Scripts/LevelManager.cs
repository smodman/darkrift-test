﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift;
using DarkRift.Client.Unity;
using DarkRift.Extension.Client;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    public Transform spawnPos;

    public UnityClient Client;

    public List<NetworkObject> levelNetworkObjects = new List<NetworkObject>();

    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Client = FindObjectOfType<UnityClient>();

        Debug.Log("Sending scene loaded message");
        Client.Client.SendMessageReliable(new SceneLoadedMessage(Client.ID, spawnPos.position));

        Destroy(gameObject);

        //using (DarkRiftWriter writer = DarkRiftWriter.Create())
        //{
        //    int[] indices = new int[levelNetworkObjects.Count];
        //    int index = 0;
        //    foreach (NetworkObject no in levelNetworkObjects)
        //    {
        //        indices[index] = index;
        //    }

        //    writer.Write(indices);

        //    // Request NetID and position information
        //    using (Message m = Message.Create(Tags.SubscribeNetworkObjects, writer))
        //    {
        //        Client.SendMessage(m, SendMode.Reliable);
        //    }
        //}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
