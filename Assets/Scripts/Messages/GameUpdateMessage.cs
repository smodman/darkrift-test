﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkRift.Extension.Message.Core;
using DarkRift;
using DarkriftSerializationExtensions;

public sealed class GameUpdateMessage : IDarkRiftMessage<ServerTag>
{
    public ServerTag Tag => ServerTag.GameUpdate;

    public ServerObject[] serverObjectData;

    public GameUpdateMessage(ServerObject[] serverObjectData)
    {
        this.serverObjectData = serverObjectData;
    }

    public GameUpdateMessage()
    {
    }

    public void Deserialize(DeserializeEvent e)
    {
        this.serverObjectData = e.Reader.ReadSerializables<ServerObject>();
    }

    public void Serialize(SerializeEvent e)
    {
        e.Writer.Write(serverObjectData);
    }
}