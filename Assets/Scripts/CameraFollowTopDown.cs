﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowTopDown : MonoBehaviour {

    public Transform rotationPivot;
    public GameObject target;

    public float yOffset = 15.0f;
    public float zOffset = -10.0f;
    private bool centerToggle;

	// Use this for initialization
	void Start () {
	}

    private void Update()
    {

    }

    // Update is called once per frame
    void LateUpdate () {

        if (target == null)
        {
            return;
        }

        rotationPivot.transform.position = target.transform.position;
        rotationPivot.transform.rotation = target.transform.rotation;

        if (Input.GetKeyDown(KeyCode.C))
        {
            centerToggle = !centerToggle;
        }

        float currentZOffset = centerToggle ? -15.0f : zOffset;

        transform.localPosition = new Vector3(0.0f, yOffset, currentZOffset);
    }
}
