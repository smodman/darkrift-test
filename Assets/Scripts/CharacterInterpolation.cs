﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NetworkObject))]
public class CharacterInterpolation : MonoBehaviour
{
    private float lastPositionTime;

    private Vector3 newPosition;
    private Vector3 lastPosition;
    NetworkObject no;

    // Start is called before the first frame update
    void Start()
    {
        newPosition = transform.position;
        lastPosition = transform.position;
        no = GetComponent<NetworkObject>();
    }

    public void MoveToPosition(Vector3 position)
    {
        lastPosition = newPosition;
        newPosition = position;
        lastPositionTime = Time.fixedTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (no.isOwner)
        {
            return;
        }

        float timeSinceLastPosition = Time.time - lastPositionTime;
        float t = timeSinceLastPosition / Time.fixedDeltaTime;
        transform.position = Vector3.Lerp(lastPosition, newPosition, t);
    }
}
